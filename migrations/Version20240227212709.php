<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240227212709 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE comment (comment_id INT AUTO_INCREMENT NOT NULL, comment_author INT NOT NULL, comment_post INT NOT NULL, comment_content LONGTEXT NOT NULL, comment_date DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', INDEX IDX_9474526C77BC80DA (comment_author), INDEX IDX_9474526C7CBCCE61 (comment_post), PRIMARY KEY(comment_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE post (post_id INT AUTO_INCREMENT NOT NULL, post_author INT NOT NULL, post_title VARCHAR(500) NOT NULL, post_content LONGTEXT NOT NULL, post_created_date DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', post_published_date DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_5A8A6C8D8D8D3CAD (post_author), PRIMARY KEY(post_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (user_id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, user_roles JSON NOT NULL COMMENT \'(DC2Type:json)\', user_password VARCHAR(255) NOT NULL, user_first_name VARCHAR(255) NOT NULL, user_last_name VARCHAR(255) NOT NULL, user_surname VARCHAR(255) NOT NULL, user_email VARCHAR(255) NOT NULL, user_phone VARCHAR(255) NOT NULL, user_social_link VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), PRIMARY KEY(user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C77BC80DA FOREIGN KEY (comment_author) REFERENCES user (user_id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C7CBCCE61 FOREIGN KEY (comment_post) REFERENCES post (post_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE post ADD CONSTRAINT FK_5A8A6C8D8D8D3CAD FOREIGN KEY (post_author) REFERENCES user (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C77BC80DA');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C7CBCCE61');
        $this->addSql('ALTER TABLE post DROP FOREIGN KEY FK_5A8A6C8D8D8D3CAD');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE post');
        $this->addSql('DROP TABLE user');
    }
}
