<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Post>
 *
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    public function findAll(): array
    {
        return $this->findBy([], ['postPublishedDate' => 'DESC']);
    }

    public function findApproved()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('p')
            ->from(Post::class, 'p')
            ->where($qb->expr()->eq('p.postApprove', $qb->expr()->literal(true)))
            ->andWhere('p.postPublishedDate is not NULL')
            ->andWhere('p.postDeletedDate is NULL')
            ->orderBy('p.postPublishedDate ', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function findAllUsersPosts($username)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('p')
            ->from(Post::class, 'p')
            ->join('p.postAuthor', 'u')
            ->where($qb->expr()->eq('u.username', $qb->expr()->literal($username)))
            ->andWhere('p.postPublishedDate is not NULL')
            ->andWhere('p.postDeletedDate is NULL')
            ->andWhere($qb->expr()->eq('p.postApprove', $qb->expr()->literal(true)))
            ->orderBy('p.postPublishedDate ', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function findNotApproved()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('p')
            ->from(Post::class, 'p')
            ->where($qb->expr()->eq('p.postApprove', $qb->expr()->literal(false)))
            ->andWhere('p.postPublishedDate is not NULL')
            ->andWhere('p.postDeletedDate is NULL')
            ->orderBy('p.postPublishedDate ', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function findNotPublished($username)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('p')
            ->from(Post::class, 'p')
            ->join('p.postAuthor', 'u')
            ->where('p.postPublishedDate is NULL')
            ->andWhere($qb->expr()->eq('u.username', $qb->expr()->literal($username)))
            ->andWhere($qb->expr()->eq('p.postApprove', $qb->expr()->literal(false)))
            ->andWhere('p.postDeletedDate is NULL')
            ->orderBy('p.postCreatedDate ', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function findPostsInCheck($username)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('p')
            ->from(Post::class, 'p')
            ->join('p.postAuthor', 'u')
            ->where($qb->expr()->eq('u.username', $qb->expr()->literal($username)))
            ->andWhere('p.postPublishedDate is not NULL')
            ->andWhere($qb->expr()->eq('p.postApprove', $qb->expr()->literal(false)))
            ->andWhere('p.postDeletedDate is NULL')
            ->orderBy('p.postPublishedDate ', 'DESC');

        return $qb->getQuery()->getResult();
    }
    //    /**
    //     * @return Post[] Returns an array of Post objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('p.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Post
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
