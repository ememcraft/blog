<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/{_locale}/user')]
class UserController extends AbstractController
{
    #[Route('/', name: 'app_user_index', methods: ['GET'])]
    #[IsGranted('ROLE_MODERATOR')]
    public function index(UserRepository $userRepository, Request $request): Response
    {
        // $this->denyAccessUnlessGranted('ROLE_MODERATOR');

        if ($request->get('username')) {
            return $this->render('user/index.html.twig', [
                'users' => $userRepository->findBy(['username' => $request->get('username')]),
            ]);
        }

        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    #[Route('/not/approved/users', name: 'app_not_approved_users', methods: ['GET'])]
    public function notApprovedUsers(UserRepository $userRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_MODERATOR');

        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findNotApprovedUsers(),
        ]);
    }

    //    #[Route('/new', name: 'app_user_new', methods: ['GET', 'POST'])]
    //    public function new(Request $request, EntityManagerInterface $entityManager): Response
    //    {
    //        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN');
    //        $user = new User();
    //        $form = $this->createForm(UserType::class, $user);
    //        $form->handleRequest($request);
    //
    //        if ($form->isSubmitted() && $form->isValid()) {
    //            $user->setUserRoles(['ROLE_UNDEFINED']);
    //            $user->setUserApprove(false);
    //            $entityManager->persist($user);
    //            $entityManager->flush();
    //
    //            return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
    //        }
    //
    //        return $this->render('user/new.html.twig', [
    //            'user' => $user,
    //            'form' => $form,
    //        ]);
    //    }

    #[Route('/{userId}', name: 'app_user_show', methods: ['GET'])]
    public function show(User $user): Response
    {
        //        $this->denyAccessUnlessGranted('ROLE_MODERATOR');

        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    #[Route('/{userId}/edit', name: 'app_user_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, User $user, EntityManagerInterface $entityManager, LoggerInterface $adminChannelLogger,
        UserPasswordHasherInterface $userPasswordHasher, UserRepository $userRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN');
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('userPassword')->getData()) {
                $userRepository->upgradePassword($user, $userPasswordHasher->hashPassword($user, $form->get('userPassword')->getData()));
            }

            $entityManager->flush();

            $adminChannelLogger->info('User @{userName} changed profile of user @{userAccount}', [
                'userName' => $this->getUser(),
                'userRole' => $user->getRoles(),
                'userAccount' => $user->getUsername(),
            ]);

            return $this->redirectToRoute('app_user_show', ['userId' => $user->getUserId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }

    #[Route('/{userId}/set/role/admin', name: 'app_user_role_super_admin', methods: ['GET', 'POST'])]
    public function updateUserRoleSuperAdmin(Request $request, User $user, EntityManagerInterface $entityManager, LoggerInterface $adminChannelLogger)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN');
        $user->setUserRoles(['ROLE_SUPER_ADMIN']);
        $entityManager->persist($user);
        $entityManager->flush();
        $adminChannelLogger->info('User @{userName} changed role "{userRole}" of user @{userAccount}', [
            'userName' => $this->getUser(),
            'userRole' => $user->getRoles(),
            'userAccount' => $user->getUsername(),
        ]);

        return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{userId}/set/role/moderator', name: 'app_user_role_moderator', methods: ['GET', 'POST'])]
    public function updateUserRoleModerator(Request $request, User $user, EntityManagerInterface $entityManager, LoggerInterface $adminChannelLogger)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN');
        $user->setUserRoles(['ROLE_MODERATOR']);
        $entityManager->persist($user);
        $entityManager->flush();
        $adminChannelLogger->info('User @{userName} changed role "{userRole}" of user @{userAccount}', [
            'userName' => $this->getUser(),
            'userRole' => $user->getRoles(),
            'userAccount' => $user->getUsername(),
        ]);

        return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{userId}/set/role/user', name: 'app_user_role_user', methods: ['GET', 'POST'])]
    public function updateUserRoleUser(Request $request, User $user, EntityManagerInterface $entityManager, LoggerInterface $adminChannelLogger)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN');
        $user->setUserRoles(['ROLE_USER']);
        $entityManager->persist($user);
        $entityManager->flush();
        $adminChannelLogger->info('User @{userName} changed role "{userRole}" of user @{userAccount}', [
            'userName' => $this->getUser(),
            'userRole' => $user->getRoles(),
            'userAccount' => $user->getUsername(),
        ]);

        return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/user/{userId}/approve', name: 'app_user_approve', methods: ['GET', 'POST'])]
    public function approve(Request $request, User $user, EntityManagerInterface $entityManager, LoggerInterface $adminChannelLogger): Response
    {
        $this->denyAccessUnlessGranted('ROLE_MODERATOR');
        $user->setUserRoles(['ROLE_USER']);
        $user->setUserApprove(true);
        $entityManager->flush();
        $adminChannelLogger->info('User @{userName} approved an account of user @{userAccount}', [
            'userName' => $this->getUser(),
            'userAccount' => $user->getUsername(),
        ]);

        return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
    }

    //    #[Route('/user/{userId}/ban', name: 'app_user_ban', methods: ['GET', 'POST'])]
    //    public function ban(Request $request, User $user, EntityManagerInterface $entityManager): Response
    //    {
    //        $this->denyAccessUnlessGranted('ROLE_MODERATOR');
    //        $user->setUserRoles(['ROLE_UNDEFINED']);
    //        $user->setUserApprove(false);
    //        $entityManager->flush();
    //
    //        return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
    //    }

    #[Route('/{userId}', name: 'app_user_delete', methods: ['POST'])]
    public function delete(Request $request, User $user, EntityManagerInterface $entityManager, LoggerInterface $adminChannelLogger): Response
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN');
        if ($this->isCsrfTokenValid('delete'.$user->getUserid(), $request->request->get('_token'))) {
            $entityManager->remove($user);
            $entityManager->flush();
            $adminChannelLogger->info('User @{userName} deleted an account of user @{userAccount}', [
                'userName' => $this->getUser(),
                'userAccount' => $user->getUsername(),
            ]);
        }

        return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
    }
}
