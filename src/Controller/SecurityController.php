<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

#[Route('/{_locale}')]
class SecurityController extends AbstractController
{
    #[Route(path: '/login', name: 'app_login')]
    public function login(AuthenticationUtils $authenticationUtils, LoggerInterface $adminChannelLogger): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        $adminChannelLogger->info('User @{userName} logged in', [
            'userName' => $authenticationUtils->getLastUsername(),
        ]);

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    #[Route(path: '/logout', name: 'app_logout')]
    public function logout(AuthenticationUtils $authenticationUtils, LoggerInterface $adminChannelLogger): void
    {
        //        $adminChannelLogger->info('User @{userName} logged out', [
        //            'userName' => $authenticationUtils->getLastUsername(),
        //        ]);

        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
