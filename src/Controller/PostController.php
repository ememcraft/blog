<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/{_locale}')]
class PostController extends AbstractController
{
    #[Route('/', name: 'app_post_index', methods: ['GET'])]
    public function index(Request $request, PostRepository $postRepository,
        LoggerInterface $adminChannelLogger, TranslatorInterface $translator): Response
    {
        if ($request->get('username')) {
            return $this->render('post/index.html.twig', [
                'posts' => $postRepository->findAllUsersPosts($request->get('username')),
            ]);
        }

        return $this->render('post/index.html.twig', [
            'posts' => $postRepository->findApproved(),
        ]);
    }

    #[Route('/drafts', name: 'app_post_drafts', methods: ['GET'])]
    public function drafts(Request $request, PostRepository $postRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        return $this->render('post/drafts.html.twig', [
                'posts' => $postRepository->findNotPublished($this->getUser()), ]);
    }

    #[Route('/not/published', name: 'app_post_not_published', methods: ['GET'])]
    public function notPublished(Request $request, PostRepository $postRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_MODERATOR');

        return $this->render('post/not_published.html.twig', [
            'posts' => $postRepository->findNotApproved(),
        ]);
    }

    #[Route('/in/check', name: 'app_post_in_check', methods: ['GET'])]
    public function postsInCheck(Request $request, PostRepository $postRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        return $this->render('post/posts_in_check.html.twig', [
            'posts' => $postRepository->findPostsInCheck($this->getUser()), ]);
    }

    #[Route('/post/new', name: 'app_post_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post->setPostAuthor($this->getUser());
            $post->setPostCreatedDate();
            $post->setPostApprove(false);
            $entityManager->persist($post);
            $entityManager->flush();

            return $this->redirectToRoute('app_post_drafts', [$this->getUser()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('post/new.html.twig', [
            'post' => $post,
            'form' => $form,
        ]);
    }

    #[Route('/post/{postId}', name: 'app_post_show', methods: ['GET'])]
    public function show(Post $post): Response
    {
        return $this->render('post/show.html.twig', [
            'post' => $post,
        ]);
    }

    #[Route('/post/{postId}/edit', name: 'app_post_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Post $post, EntityManagerInterface $entityManager, PostRepository $postRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        if ($this->getUser() == $post->getPostAuthor()->getUsername() or in_array('ROLE_SUPER_ADMIN', $this->getUser()->getRoles(), true)) {
            $form = $this->createForm(PostType::class, $post);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $post->setPostApprove(false);
                $entityManager->flush();

                return $this->render('post/show.html.twig', [
                    'post' => $post,
                ]);
            }

            return $this->render('post/edit.html.twig', [
                'post' => $post,
                'form' => $form,
            ]);
        }

        return $this->render('post/index.html.twig', [
            'posts' => $postRepository->findApproved(),
        ]);
    }

    #[Route('/post/{postId}/publish', name: 'app_post_publish', methods: ['GET', 'POST'])]
    public function publish(Request $request, Post $post, EntityManagerInterface $entityManager, LoggerInterface $adminChannelLogger): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        if ($this->getUser() == $post->getPostAuthor()->getUsername()) {
            $post->setPostPublishedDate();
            $entityManager->flush();
            $adminChannelLogger->info('User @{postAuthor} created a post "{postTitle}"', [
                'postAuthor' => $this->getUser(),
                'postTitle' => $post->getPostTitle(),
            ]);
        }

        return $this->redirectToRoute('app_post_drafts', [$this->getUser()], Response::HTTP_SEE_OTHER);
    }

    #[Route('/post/{postId}/approve', name: 'app_post_approve', methods: ['GET', 'POST'])]
    public function approve(Request $request, Post $post, EntityManagerInterface $entityManager, LoggerInterface $adminChannelLogger, MailerInterface $mailer): Response
    {
        $this->denyAccessUnlessGranted('ROLE_MODERATOR');
        $post->setPostPublishedDate();
        $post->setPostApprove(true);
        $entityManager->flush();
        $adminChannelLogger->info('User @{userName} approved a post "{postTitle}" of user @{postAuthor}', [
            'userName' => $this->getUser(),
            'postAuthor' => $post->getPostAuthor()->getUsername(),
            'postTitle' => $post->getPostTitle(),
        ]);

        $email = (new Email())
            ->from('mailtrap@demomailtrap.com')
            ->to('ememcraft@gmail.com')
            ->subject('Your post was approved!')
            ->text('Hello, {postAuthor}. Your post "{postTitle}" was approved on {postPublishedDate} by {userName}! Check it!');
        //            ->html('<p>See Twig integration for better HTML integration!</p>');

        $mailer->send($email);

        return $this->redirectToRoute('app_post_not_published', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/post/{postId}', name: 'app_post_delete', methods: ['POST'])]
    public function delete(Request $request, Post $post, EntityManagerInterface $entityManager, LoggerInterface $adminChannelLogger): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        if ($this->getUser() == $post->getPostAuthor()->getUsername() or in_array('ROLE_SUPER_ADMIN', $this->getUser()->getRoles(), true)) {
            if ($this->isCsrfTokenValid('delete'.$post->getPostId(), $request->request->get('_token'))) {
                $post->setPostDeletedDate();
                $entityManager->flush();
                $adminChannelLogger->info('User @{userName} deleted a post "{postTitle}" of user @{postAuthor}', [
                    'userName' => $this->getUser(),
                    'postAuthor' => $post->getPostAuthor()->getUsername(),
                    'postTitle' => $post->getPostTitle(),
                ]);
            }
        }

        return $this->redirectToRoute('app_post_index', [], Response::HTTP_SEE_OTHER);
    }
}
