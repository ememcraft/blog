<?php

namespace App\Entity;

use App\Repository\CommentRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: CommentRepository::class)]
class Comment
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'comment_id')]
    private ?int $commentId = null;

    #[Assert\NotBlank]
    #[ORM\Column(name: 'comment_content', type: Types::TEXT)]
    private ?string $commentContent = null;

    #[ORM\Column(name: 'comment_date', type: 'datetime_immutable', nullable: false)]
    private ?\DateTimeImmutable $commentDate;

    #[ORM\ManyToOne(targetEntity: 'User', inversedBy: 'authors_comments')]
    #[ORM\JoinColumn(name: 'comment_author', referencedColumnName: 'user_id', nullable: false)]
    private ?User $commentAuthor = null;

    #[ORM\ManyToOne(targetEntity: 'Post', inversedBy: 'post_comments')]
    #[ORM\JoinColumn(name: 'comment_post', referencedColumnName: 'post_id', onDelete: 'cascade', nullable: false)]
    private ?Post $commentPost = null;

    public function __construct()
    {
        $this->commentDate = new \DateTimeImmutable();
    }

    public function getCommentId(): ?int
    {
        return $this->commentId;
    }

    public function getCommentContent(): ?string
    {
        return $this->commentContent;
    }

    public function setCommentContent(string $commentContent): static
    {
        $this->commentContent = $commentContent;

        return $this;
    }

    public function getCommentDate(): ?\DateTimeImmutable
    {
        return $this->commentDate;
    }

    public function setCommentDate(): void
    {
        $this->commentDate = new \DateTimeImmutable();
    }

    public function getCommentAuthor(): ?User
    {
        return $this->commentAuthor;
    }

    public function setCommentAuthor(?User $commentAuthor): static
    {
        $this->commentAuthor = $commentAuthor;

        return $this;
    }

    public function getCommentPost(): ?Post
    {
        return $this->commentPost;
    }

    public function setCommentPost(?Post $commentPost): static
    {
        $this->commentPost = $commentPost;

        return $this;
    }
}
