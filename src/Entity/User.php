<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: UserRepository::class)]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $userId = null;

    #[Assert\NotBlank]
    #[ORM\Column(name: 'username', length: 180, unique: true, nullable: false)]
    private ?string $username = null;

    /**
     * @var list<string> The user roles
     */
    #[ORM\Column(name: 'user_roles')]
    private array $userRoles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column(name: 'user_password')]
    private ?string $userPassword = null;
    #[Assert\NotBlank]
    #[ORM\Column(name: 'user_first_name', length: 255, nullable: false)]
    private ?string $userFirstName = null;
    #[Assert\NotBlank]
    #[ORM\Column(name: 'user_last_name', length: 255, nullable: false)]
    private ?string $userLastName = null;
    #[Assert\NotBlank]
    #[ORM\Column(name: 'user_surname', length: 255, nullable: false)]
    private ?string $userSurname = null;
    #[Assert\Email]
    #[Assert\NotBlank]
    #[ORM\Column(name: 'user_email', length: 255, nullable: false)]
    private ?string $userEmail = null;
    #[Assert\NotBlank]
    #[ORM\Column(name: 'user_phone', length: 255, nullable: false)]
    private ?string $userPhone = null;

    #[ORM\Column(name: 'user_social_link', length: 255, nullable: true)]
    private ?string $userSocialLink = null;

    #[ORM\OneToMany(targetEntity: Post::class, mappedBy: 'post_author')]
    private Collection $authorsPosts;

    #[ORM\OneToMany(targetEntity: Comment::class, mappedBy: 'comment_author')]
    private Collection $authorsComments;

    #[ORM\Column(name: 'user_approve', type: 'boolean', nullable: false)]
    private ?bool $userApprove = false;

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): static
    {
        $this->username = $username;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     *
     * @return list<string>
     */
    public function getRoles(): array
    {
        $user_roles = $this->userRoles;
        // guarantee every user at least has ROLE_USER
        if (0 === count($user_roles)) {
            return ['ROLE_UNDEFINED'];
        }

        return array_unique($user_roles);
    }

    /**
     * @param list<string> $userRoles
     */
    public function setUserRoles(array $userRoles): static
    {
        $this->userRoles = $userRoles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->userPassword;
    }

    public function setUserPassword(string $userPassword): static
    {
        $this->userPassword = $userPassword;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getUserFirstName(): ?string
    {
        return $this->userFirstName;
    }

    public function setUserFirstName(?string $userFirstName): static
    {
        $this->userFirstName = $userFirstName;

        return $this;
    }

    public function getUserLastName(): ?string
    {
        return $this->userLastName;
    }

    public function setUserLastName(?string $userLastName): static
    {
        $this->userLastName = $userLastName;

        return $this;
    }

    public function getUserSurname(): ?string
    {
        return $this->userSurname;
    }

    public function setUserSurname(?string $userSurname): static
    {
        $this->userSurname = $userSurname;

        return $this;
    }

    public function getUserPhone(): ?string
    {
        return $this->userPhone;
    }

    public function setUserPhone(?string $userPhone): static
    {
        $this->userPhone = $userPhone;

        return $this;
    }

    public function getUserEmail(): ?string
    {
        return $this->userEmail;
    }

    public function setUserEmail(?string $userEmail): static
    {
        $this->userEmail = $userEmail;

        return $this;
    }

    public function getUserSocialLink(): ?string
    {
        return $this->userSocialLink;
    }

    public function setUserSocialLink(string $userSocialLink): static
    {
        $this->userSocialLink = $userSocialLink;

        return $this;
    }

    /**
     * @return Collection<int, Post>
     */
    public function getAuthorsPosts(): Collection
    {
        return $this->authorsPosts;
    }

    public function addAuthorPost(Post $authorPost): static
    {
        if (!$this->authorsPosts->contains($authorPost)) {
            $this->authorsPosts->add($authorPost);
            $authorPost->setPostAuthor($this);
        }

        return $this;
    }

    public function removeAuthorsPosts(Post $authorPost): static
    {
        if ($this->authorsPosts->removeElement($authorPost)) {
            // set the owning side to null (unless already changed)
            if ($authorPost->getPostAuthor() === $this) {
                $authorPost->setPostAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getAuthorsComments(): Collection
    {
        return $this->authorsComments;
    }

    public function addAuthorComment(Comment $authorComment): static
    {
        if (!$this->authorsComments->contains($authorComment)) {
            $this->authorsComments->add($authorComment);
            $authorComment->setCommentAuthor($this);
        }

        return $this;
    }

    public function removeAuthorComment(Comment $authorComment): static
    {
        if ($this->authorsComments->removeElement($authorComment)) {
            // set the owning side to null (unless already changed)
            if ($authorComment->getCommentAuthor() === $this) {
                $authorComment->setCommentAuthor(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getUsername();
    }

    public function isUserApprove(): ?bool
    {
        return $this->userApprove;
    }

    public function getUserApprove(bool $userApprove): ?bool
    {
        return $this->userApprove;
    }

    public function setUserApprove(bool $userApprove): static
    {
        $this->userApprove = $userApprove;

        return $this;
    }
}
