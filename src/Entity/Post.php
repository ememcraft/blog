<?php

namespace App\Entity;

use App\Repository\PostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Validator\Constraints as Assert;

#[Route('{_locale<%app.supported_locales%>}')]
#[ORM\Entity(repositoryClass: PostRepository::class)]
class Post
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'post_id')]
    private ?int $postId = null;

    #[Assert\NotBlank]
    #[ORM\Column(name: 'post_title', length: 500, nullable: false)]
    private ?string $postTitle = '';

    #[Assert\NotBlank]
    #[ORM\Column(type: Types::TEXT)]
    private ?string $postContent = null;

    #[ORM\Column(name: 'post_created_date', type: 'datetime_immutable', nullable: false)]
    private ?\DateTimeImmutable $postCreatedDate;

    #[ORM\Column(name: 'post_published_date', type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $postPublishedDate = null;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'post_author', referencedColumnName: 'user_id', nullable: false)]
    private ?User $postAuthor = null;

    #[ORM\OneToMany(targetEntity: Comment::class, mappedBy: 'commentPost')]
    private Collection $postComments;

    #[ORM\Column(name: 'post_approve', type: 'boolean', nullable: 'false')]
    private ?bool $postApprove = null;

    #[ORM\Column(name: 'post_deleted_date', type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $postDeletedDate = null;

    public function getPostId()
    {
        return $this->postId;
    }

    public function __construct()
    {
        $this->postCreatedDate = new \DateTimeImmutable();
        $this->postComments = new ArrayCollection();
    }

    public function getPostTitle(): ?string
    {
        return $this->postTitle;
    }

    public function setPostTitle(?string $postTitle): static
    {
        $this->postTitle = $postTitle;

        return $this;
    }

    public function getPostContent(): ?string
    {
        return $this->postContent;
    }

    public function setPostContent(?string $postContent): static
    {
        $this->postContent = $postContent;

        return $this;
    }

    public function getPostCreatedDate(): ?\DateTimeImmutable
    {
        return $this->postCreatedDate;
    }

    public function setPostCreatedDate(): void
    {
        $this->postCreatedDate = new \DateTimeImmutable();
    }

    public function getPostDeletedDate(): ?\DateTimeImmutable
    {
        return $this->postDeletedDate;
    }

    public function getPostPublishedDate(): ?\DateTimeImmutable
    {
        return $this->postPublishedDate;
    }

    public function setPostPublishedDate(): void
    {
        $this->postPublishedDate = new \DateTimeImmutable();
    }

    public function setPostDeletedDate(): void
    {
        $this->postDeletedDate = new \DateTimeImmutable();
    }

    public function getPostAuthor(): ?User
    {
        return $this->postAuthor;
    }

    public function getCommentsCount()
    {
    }

    public function __toString(): string
    {
        return $this->getPostTitle();
    }

    public function setPostAuthor(?User $postAuthor): static
    {
        $this->postAuthor = $postAuthor;

        return $this;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getPostComments(): Collection
    {
        return $this->postComments;
    }

    public function addPostComment(Comment $postComment): static
    {
        if (!$this->postComments->contains($postComment)) {
            $this->postComments->add($postComment);
            $postComment->setCommentPost($this);
        }

        return $this;
    }

    public function removePostComment(Comment $postComment): static
    {
        if ($this->postComments->removeElement($postComment)) {
            // set the owning side to null (unless already changed)
            if ($postComment->getCommentPost() === $this) {
                $postComment->setCommentPost(null);
            }
        }

        return $this;
    }

    public function isPostApprove(): ?bool
    {
        return $this->postApprove;
    }

    public function setPostApprove(bool $postApprove): static
    {
        $this->postApprove = $postApprove;

        return $this;
    }
}
